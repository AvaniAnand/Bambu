const Person = require('../models/Person');
const path = require('path');
const faker = require('fakerator')();
const async = require('async');
const _ = require('lodash');
const fs = require('fs');

/* vars_weight being used to verify all possible features given in  /people-like-you endpoint */

var vars_weights = require('../weights.json');



/* endpoint for '/' */
exports.getIndex = (req, res) => {
	var count = Person.count({}, (err, count) => {
		if(err){
			res.json({success : true, msg : `Welcome to Bambu.life`});
		}
		else{
			res.json({success : true, msg : `Welcome to Bambu.life . Total Observations : ${count}` });
		}
	});	
}

/* endpoint for /person/add?name=ajay&age=25 ; adds 1 person based on query values */
exports.getAddPerson = (req, res)=> {
	var form = req.query;
	var count = 1;
	var person = new Person({
		name : form.name,
		age : form.age,
		latitude : form.latitude,
		longitude : form.longitude,
		monthlyIncome : form.monthlyIncome,
		experienced : form.experienced
	});
	person.save((err, person_1) => {
		console.log(err, person_1);
		if(err){
			res.json({success : false, msg : err.toString()});
		}
		else{
			res.json({success : true, msg : 'successfully added 1 person'});
		}
	});
}

/* endpoint for /person/random?count=N ; adds N random person data  */
exports.getAddRandom = (req, res) => {
	var form = req.query;
	var count = (form.count ? form.count : 50);
	console.log(count);
	var errors = [];
	async.whilst(
		()=> { console.log(count); return count > 0;}, 
		(cb)=> {
			var fName = faker.names.firstName();
			var geoC = faker.address.geoLocation();
			var person = new Person({
				name : fName,
				age : faker.random.number(18, 100),
				latitude : geoC.latitude.toString(),
				longitude : geoC.longitude.toString(),
				monthlyIncome : faker.random.number(1000, 40000),
				experienced : faker.random.boolean()				
			});
			console.log(person);
			person.save((err, person_1) => {
				console.log(err, person_1);
				if(err){
					errors.push({'email' : person.email, msg : err.toString()});
					count--;
					cb(null, count);
				}
				else{
					count--;
					cb(null, count);
				}
			});
		},
		(err, n)=> {
			if(err){
				res.json({success : false, msg : 'error while adding random persons'});
			}
			else{
				var done = form.count - errors.length;
				res.json({success : true, msg : `successfully added ${done} persons`, errors : errors});
			}
		}
	);
};


exports.getChangeWeights = (req,  res) => {
	var form = req.query;
	var match_vars = _.keys(form).every(function(val) { return _.keys(vars_weights).indexOf(val) >= 0; });
	if(!match_vars){
		res.json({success : false, error : 'Variables given in query request mismatches with original vars. Plz check spellings.'});
	}else{
		async.each(_.keys(form), (key, cb)=>{
			vars_weights[key] = parseFloat(form[key]);
			cb();
		}, (err)=>{
			if(err){res.json({success : false, error : err.message})}
			else{
				vars_weights['longitude'] = vars_weights['latitude'];
				var json = JSON.stringify(vars_weights, null, 2);
				var content = fs.writeFileSync('weights.json', json);
				res.json({success : true, vars_weights : vars_weights});
				// ,  'utf8', function (err) {
				//   if (err) {console.log(err); res.json({success : false, error : err.message}) }
				//   else{
				//   	console.log(JSON.stringify(vars_weights, null, 2));
					
				//   }
				// });
			}
		})
	}
}

/* function for calculating range for a feature eg. age */
async function get_range(feature){
	return new Promise((resolve, reject) =>{
		Person.aggregate([{ "$group": 
			{ 
				_id : null,
				"max": { "$max": "$"+feature }, 
				"min": { "$min": "$"+feature } 
			}
		}], (err, range)=>{
			if(err){
				reject(err);
			}
			else{
				console.log(range);
				resolve(Math.abs(Number(range[0].max) -Number(range[0].min)));
			}
		});
	});
}


/* function for calculating range for all features eg. age ; range = max - min; return object */
async function get_ranges(form){
	return new Promise((resolve, reject)=>{
		var response = {};
		async.forEachOf(form, (value, feature, cb)=>{
			get_range(feature).then((range)=>{
				response[feature] = range;
				cb();
			}).catch((err)=>{
				console.log(err);
				cb(err);
			})
		}, (err)=>{
			if(err){
				reject(err.message);
			}
			else{
				resolve(response);
			}
		})
	});
}


/* function for calculating stddev for a feature eg. age ; range = max - min */
async function get_stddev(feature){
	return new Promise((resolve, reject) =>{
		Person.aggregate([{ "$group": 
			{ 
				_id : null,
				"avg": { "$avg": "$"+feature }, 
				"stddev": { "$stdDevPop": "$"+feature } 
			}
		}], (err, range)=>{
			if(err){
				reject(err);
			}
			else{
				console.log(range);
				resolve(Number(range[0].stddev));
			}
		});
	});
}

/* function for calculating stddev for all features eg. age ; return object */
async function get_stddev_all(form){
	return new Promise((resolve, reject)=>{
		var response = {};
		async.forEachOf(form, (value, feature, cb)=>{
			get_stddev(feature).then((range)=>{
				response[feature] = range;
				cb();
			}).catch((err)=>{
				console.log(err);
				cb(err);
			})
		}, (err)=>{
			if(err){
				reject(err.message);
			}
			else{
				resolve(response);
			}
		})
	});
}


/*Endpoint logic for /people-like-you; 
Uses MinMax Normalization of feaures i.e [0, 1] scaling ; 
Used MinMax approach to account for Categorical "experienced" feature , geo -location feature with other continuous features) */
exports.getPeopleLikeYou = (req, res) => {
	var form = req.query;
	var num_vars = _.keys(form).length;
	var cut_off_score = 0; // cut_off for confidence score
	var match_vars = _.keys(form).every(function(val) { return _.keys(vars_weights).indexOf(val) >= 0; });

	if(form.latitude && form.longitude){
		num_vars--; // assuming latitude & longitude as 1 feature
	}
	console.log(form);
	if(num_vars == 0){
		res.json({'success' : false, error : 'No variables to match against. Please input atleast 1 variable'});
	}
	else if(!match_vars){
		res.json({'success' : false, error : 'Variables given in query request mismatches with original vars. Plz check spellings.'});
	}
	else if(form.experienced && (form.experienced != 'true' && form.experienced != 'false')){
		res.json({'success' : false, error : 'Please make sure - experienced is in boolean format i.e. true or false.'});
	}
	else if((form.latitude && !parseFloat(form.latitude)) || (form.longitude && !parseFloat(form.longitude)) || (form.age && !parseFloat(form.age)) || (form.monthlyIncome && !parseFloat(form.monthlyIncome))){
		res.json({'success' : false, error : 'Please make sure - latitude , longitude , age , monthlyIncome are in Number Format.'});
	}
	else if ((form.latitude && !form.longitude) || (!form.latitude && form.longitude)){
		res.json({'success' : false, error : 'Need both latitude & longitude for location.'});
	}
	else if (form.latitude && (parseFloat(form.latitude) < -90 || parseFloat(form.latitude) > 90)){
		res.json({'success' : false, error : 'Latitude should be within -90 to 90'});
	}
	else if (form.longitude && (parseFloat(form.longitude) < -180 || parseFloat(form.longitude) > 180)){
		res.json({'success' : false, error : 'Longitude should be within -180 to 180'});
	}
	else {
		if(form.experienced){
			form.experienced = JSON.parse(form.experienced);
		}
		var keys = _.keys(form);
		Person.findOne({}).exec((err, person1)=>{
			if(err){
				res.json({'success' : false, error : err.message})
			}
			else if (!person1){
				res.json({ peopleLikeYou : []})
			}
			else{
				get_ranges(form).then((range)=> {
					console.log(range);
					Person.aggregate(
						[
							{
								// get the distance from input point in meters using geoNear function in MongoDB -- must have geoIndexing in mongodb
								$geoNear : {
									query : { 'latitude' : { $exists: true } },
									near : { type : "Point", coordinates : [parseFloat(form.longitude) || 0.001, parseFloat(form.latitude) || 0.001]},
									distanceField : 'distance',
									spherical : true
								}
							}
						, {
							// add "Score" field to find Nearest neighbours in DB
							// example - only 2 features used for NN problem with , d1 within [0 -1] and w1 weights , d2 within [0-1] and w2 weights
							// score = 1- (sqrt(w1*w1*d1*d1 + w2*w2*d2*d2)/sqrt(w1*w1 + w2*w2)
							$addFields : { 
								"score" : {
									// subtract from 1, the score (euclidean distance) of person wrt input  ( simple transformation to achieve confidence based on distance) 
									$subtract : [ 1, {
										$sqrt : {
											$divide : [
												{
													$add : [
														{ $cond : { if: { $in : ['age' , keys ] }   , then: {
																$pow : [
																	{	
																		$multiply : [
																			vars_weights['age'] , 
																			{
																				$divide : [ { $abs : { $subtract : ["$age" , Number(form.age)]}}, Number(range.age)]
																			}
																		]
																	}
																	, 2
																]
															}, else: 0 }
														},
														{ $cond : { if: { $in : ['monthlyIncome' , keys ] }    , then: {
																$pow : [
																	{	
																		$multiply : [
																			vars_weights['monthlyIncome'] , 
																			{
																				$divide : [ { $abs : { $subtract : ["$monthlyIncome" , Number(form.monthlyIncome)]}}, Number(range.monthlyIncome)]
																			}
																		]
																	}
																	, 2
																]
															}, else: 0 }
														},
														{ $cond : { if: { $in : ['experienced' , keys ] }   , then: {
																$pow : [
																	{	
																		$multiply : [
																			vars_weights['experienced'] , 
																			{
																				$divide : [ { $abs : { $subtract : [ { $cond : [ "$experienced", 1, 0 ] } , Number(form.experienced)]}}, Number(range.experienced)]
																			}
																		]
																	}, 2
																]
															}, else: 0 }
														},
														{ $cond : { if: { $in : ['latitude' , keys ] }   , then: {
																$pow : [
																	{	
																		$multiply : [
																			vars_weights['latitude'] , 
																			{
																				$divide : [ '$distance', Number(6378137*Math.PI)]
																			}
																		]
																	}, 2
																]
															}, else: 0 }
														},
													]
												} , 
												{
													$add : [
														{ $cond : { if: { $in : ['age' , keys ] }   , then: { $multiply : [vars_weights['age'], vars_weights['age']] } , else : 0 } },
														{ $cond : { if: { $in : ['monthlyIncome' , keys ] }   , then: {$multiply : [vars_weights['monthlyIncome'], vars_weights['monthlyIncome']] } , else : 0 } },
														{ $cond : { if: { $in : ['experienced' , keys ] }   , then: {$multiply : [vars_weights['experienced'], vars_weights['experienced']] } , else : 0 } },
														{ $cond : { if: { $in : ['latitude' , keys ] }   , then: {$multiply : [vars_weights['latitude'], vars_weights['latitude']] } , else : 0 } }
													]
												}
											]
										}
									}] 
								}
							} 
						}] , 
						(err, data) =>{
							if(err){
								res.json({success: false, error : err.message});
							}
							else{
								var final = data
									.filter((a)=> { return a.score > 0})
									.sort((a,b) => {return b.score-a.score}).slice(0,10)
									.map((person)=> {
										return {
											name : person.name,
				 							age : person.age,
				 							latitude : person.latitude,
				 							longitude : person.longitude,
				 							monthlyIncome : person.monthlyIncome,
				 							experienced : person.experienced,
				 							score : person.score
										}
									});
								res.json({ peopleLikeYou : final });
							}
						}
					);
				}).catch((err)=>{
					console.log(err);
					res.json({success: false, error : err.message});
				});	
			}
		})
		
	}
	
};




/* Code to upload Data into MongoDB */

// const csv = require("csvtojson")();
// const dataFile = path.join(__dirname, '../data.csv');
// console.log(dataFile);

// Person.remove();

// csv
// .fromFile(dataFile)
// .then((jsonObj)=>{
// 	var count = jsonObj.length;
// 	var i = 0;
// 	async.whilst(
// 		()=> { console.log(i); return i < count;},
// 		(cb) => {
// 			var form = jsonObj[i];
// 			var person = new Person({
// 				name : form.name,
// 				age : Number(form.age),
// 				latitude : form.latitude,
// 				longitude : form.longitude,
// 				monthlyIncome : parseFloat(form.monthlyIncome),
// 				experienced : JSON.parse(form.experienced)
// 			});
// 			person.save((err, person_1) => {
// 				//console.log(err, person_1);
// 				if(err){
// 					cb(err);
// 				}
// 				else{
// 					i++;
// 					cb();
// 				}
// 			});
// 		},
// 		(err)=>{
// 			console.log(err);
// 		}
// 	)
// })



