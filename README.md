
Finding Nearest Neighbour API


Backend Technologies :
	1. NodeJS (version:8.9.3)
	2. MongoDB (version:3.4)
	3. NPM (version:5.5.1)


Endpoints :

	1. "/"
		-- 
			Index
	2. "/people-like-you?query"
		-- 
			Api for finding nearest neighbour from database using input query.
				Logic is based on creating scores for each person in database  using input query features via MongoDB aggregation function.
				Only Input features are used to create final scores.
			Uses MinMax Normalization of feaures i.e [0, 1] scaling ;
				Xrange = [Xmax - Xmin] ( calculated in runtime from DB)
				X(i)' = [X(i) - Xmin]/[Xrange]
				hence, every point on features comes within [0,1] scaling
				Reason to used MinMax approach : To account for Categorical "experienced" feature , geo -location feature with other continuous features)
			"vars_weight" in "controllers/bambu.js" 
				It is used in assigning weights to the features when combining all features together.
				It is also being used to verify all possible features given in  /people-like-you input query.
				For Example - only 2 features used for NN problem with , Feature 1 has distance score d1 normalized within [0 -1] and w1 weight , Feature 2 has distance d2 normalized within [0-1] and w2 weight.
				Score = 1- [(sqrt(w1*w1*d1*d1 + w2*w2*d2*d2)/sqrt(w1*w1 + w2*w2)]
				Results can be improved by changing the weights in "vars_weight" given to various features. *(latitude & longitude must have same weights)
				A better approach would be, supervised learning ( feedback & machine learning) to get the final weights.
				For start, we are using weights - 
					'age' : 5,
					'latitude' : 1,
					'longitude' : 1,
					'monthlyIncome' : 1,
					'experienced' : 0.5
			GeoIndexing in MongoDB
				For faster results, we are $geoNear function of MongoDB aggregation which helps in calculating geo distance between two points.
				This also requires to create geoIndex based on "location" (longitude , latitude). See models/Person.js. 
				Distance comes in metres , and is divided by Number(6378137*Math.PI) to make it scale within [0-1]
			Categorical Features
				"experienced" feature is a categorical feature. Hence, we use similarity measure to find the feature dimension.
	3. "/person/add?query"
		--
			Api to add one person from input query to database
	4. "/person/random?count=N"
		-- 
			Api to add random N person to database.
			using fakerator npm module to create dummy persons.

	5. "/change-weight?age=10"
		--
			Api to change weights assigned to features while calculating people-like-you
				For start, we are using weights - 
					'age' : 5,
					'latitude' : 1,
					'longitude' : 1,
					'monthlyIncome' : 1,
					'experienced' : 0.5





