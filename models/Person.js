const mongoose = require('mongoose');
const _ = require('lodash');
const async = require('async');
const geo_dist = require('haversine-distance');

/* Create Person Schema */
const personSchema = new mongoose.Schema({
  name: String,
  age : Number,
  latitude : String,
  longitude : String,
  monthlyIncome : Number,
  experienced : Boolean,
  location: {
        type: { type: String },
        coordinates: []
    }
}, { timestamps: true });

/* Create location field before saving document for geoIndexing */
personSchema.pre('save', function save(next) {
  const person = this;
  if(person.latitude && person.longitude){
	  	person.location = {
	  	type : 'Point',
	  	coordinates : [parseFloat(person.longitude), parseFloat(person.latitude)]
	  }
  }
  next();
});

/* Create geoIndexing */
personSchema.index({ "location": "2dsphere" });


/* Method to calc Score for Nearest Neighbour based on given form query, range of features in database  */
personSchema.methods.get_NN_score = function(form, range, num_vars, cb){
	var that = this;
	//console.log(this);
	var response = {};
	async.each(_.keys(form), (feature, cb1)=>{
		//console.log(feature, Number(that[feature]), Number(form[feature]), Number(range[feature]))
		if(feature != 'latitude' && feature != 'longitude'){
			response[feature] = (Number(Math.abs(Number(that[feature]) - Number(form[feature]))/(range[feature])));
		}
		cb1();
	}, (err)=>{
		if(err){
			cb(err);
		}
		else{
			if(form.latitude){
				response.location = geo_dist({latitude : form.latitude , longitude : form.longitude}, {latitude : this.latitude , longitude : this.longitude})/Number(6378137*Math.PI);
				//console.log('location' , response.location);
			}
			//var score = 1- _.values(response).reduce((a,b)=> a+b)/num_vars;
			var score = (1- Math.sqrt(_.values(response).map( a => Math.pow(a/num_vars, 2)).reduce((a, b) => a+b)));
			//console.log(score);
			cb(null, score);
		}
	})
};

const Person = mongoose.model('Person', personSchema);

module.exports = Person;
