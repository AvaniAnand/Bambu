process.env.NODE_ENV = 'dev';

let mongoose = require("mongoose");
let bambuController = require('../controllers/bambu');
let Person = require('../models/Person');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('bambu test env', () => {
    // beforeEach((done) => { //Before each test we empty the database
    //     Person.remove({}, (err) => { 
    //        done();         
    //     });     
    // });

    describe('/People-like-you api', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('No variables to match against. Please input atleast 1 variable');
	          done();
	        });
	  });
	});

	describe('/People-like-you latitude datatype mismatch', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?latitude=ram')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Please make sure - latitude , longitude , age , monthlyIncome are in Number Format.');
	          done();
	        });
	  });
	});

	describe('/People-like-you  longitude datatype mismatch', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?longitude=ram')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Please make sure - latitude , longitude , age , monthlyIncome are in Number Format.');
	          done();
	        });
	  });
	});

	describe('/People-like-you age datatype mismatch', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?age=ram')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Please make sure - latitude , longitude , age , monthlyIncome are in Number Format.');
	          done();
	        });
	  });
	});

	describe('/People-like-you monthlyIncome datatype mismatch', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?monthlyIncome=ram')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Please make sure - latitude , longitude , age , monthlyIncome are in Number Format.');
	          done();
	        });
	  });
	});

	describe('/People-like-you experienced datatype mismatch', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?experienced=ram')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Please make sure - experienced is in boolean format i.e. true or false.');
	          done();
	        });
	  });
	});

	describe('/People-like-you latitude within range', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?latitude=1000&longitude=40')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Latitude should be within -90 to 90');
	          done();
	        });
	  });
	});

	describe('/People-like-you longitude within range', () => {
	  it('it should return success false and error msg', (done) => {
	    chai.request(server)
	        .get('/people-like-you?latitude=40&longitude=1000')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('success').eql(false);
	            res.body.should.have.property('error').eql('Longitude should be within -180 to 180');
	          done();
	        });
	  });
	});

	describe('/People-like-you out of range', () => {
	  it('it should return object peopleLikeYou', (done) => {
	    chai.request(server)
	        .get('/people-like-you?age=1000')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('peopleLikeYou');
	            res.body.peopleLikeYou.length.should.be.eql(0);
	          done();
	        });
	  });
	});

	describe('/People-like-you in range', () => {
	  it('it should return object peopleLikeYou', (done) => {
	    chai.request(server)
	        .get('/people-like-you?age=55')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('peopleLikeYou');
	            res.body.peopleLikeYou.length.should.be.eql(10);
	          done();
	        });
	  });
	});

	describe('/People-like-you in range all vars', () => {
	  it('it should return object peopleLikeYou', (done) => {
	    chai.request(server)
	        .get('/people-like-you?age=23&latitude=40.71667&longitude=19.56667&monthlyIncome=5500&experienced=false')
	        .end((err, res) => {
	            res.should.have.status(200);
	            res.body.should.be.a('object');
	            res.body.should.have.property('peopleLikeYou');
	            res.body.peopleLikeYou.length.should.be.eql(10);
	          done();
	        });
	  });
	});
	

});


